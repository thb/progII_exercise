import java.time.DateTimeException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Die Methoden der Klasse KundeVO werden getestet.
 *
 * Zum Testen werden spezielle Assert-Befehle eingesetzt <br>
 *
 * @author Gabriele Schmidt
 * @version 1.0 28.03.2014
 */
public class KundeVOTest {

	private static KundeVO kundeDefault, kundeIni;
	private static KundeVO kundetX, kundetY, kundeZ;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		kundeDefault = new KundeVO();

		kundetX = new KundeVO("Nachname", "Vorname");

		kundeIni = new KundeVO("Nachname", "Vorname", "männlich",
				LocalDate.of(1980, 1, 31));

	}

	@Test
	public void idInKundeVO() {

		assertEquals(kundeDefault.getClass() + " hat die ID 0", 0,
				kundeDefault.getId());
		assertEquals(kundeIni.getClass() + " hat die ID 2", 2,
				kundeIni.getId());
		// die IDs werden korrekt hochgezählt
		for (int i = KundeVO.getNaechsteID(); i < kundeIni.getId() + 10; i++) {
			kundeDefault = new KundeVO();
			assertEquals(kundeDefault.getClass() + " hat die ID " + i,
					i , kundeDefault.getId());
		}
		// die vergebene Id verändert sich nicht
		assertEquals(kundeIni.getClass() + " hat die ID 1", 2,
				kundeIni.getId());
	}

	@Test
	public void setGeburtsdatumInKundeVO() {

		int alter = 31;
		LocalDate heute = LocalDate.now();
		int jahr = heute.getYear();
		int monat = heute.getMonthValue();
		int tag = heute.getDayOfMonth();

		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat im Geburtsdatum das Jahr "
				+ (jahr - alter), jahr - alter, kundeIni.getGeburtsdatum()
				.getYear());
		assertEquals(kundeIni.getClass() + " hat  im Geburtsdatum den Monat "
				+ monat, monat, kundeIni.getGeburtsdatum().getMonthValue());
		assertEquals(kundeIni.getClass() + " hat  im Geburtsdatum den Tag "
				+ tag, tag, kundeIni.getGeburtsdatum().getDayOfMonth());

		// Monat Januar
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, 1, tag));
		assertEquals(kundeIni.getClass()
				+ " hat  im Geburtsdatum den Monat Januar", 1, kundeIni
				.getGeburtsdatum().getMonthValue());

		// Setzt ein zu junges Datum
		alter = 12;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat im Geburtsdatum das Jahr "
				+ (jahr - alter) + ", ist zu jung und daher kein Objekt", null,
				kundeIni.getGeburtsdatum());

		// Setzt Alter 60
		alter = 60;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat im Geburtsdatum das Jahr "
				+ (jahr - alter) + ", ist zu alt und daher kein Objekt",LocalDate.of(jahr - alter, monat, tag),
				kundeIni.getGeburtsdatum());

		// Setzt ein Schaltjahr

		kundeIni.setGeburtsdatum(LocalDate.of(1964, 2, 29));
		assertNotNull(
				kundeIni.getClass()
						+ " hat ein Geburtsdatum 29. Februar 1964, ist daher eein Objekt",
				kundeIni.getGeburtsdatum());

	}

	// Setzte falsches Datum, z. B. 30.2.1967
	//Erwarte Exception
	@Test(expected = DateTimeException.class)
	public void setGeburtsdatumInKundeVOMitException() {
		kundeIni.setGeburtsdatum(LocalDate.of(1967, 2, 30));
	}

	@Test
	public void berechneAlterInKundeVO() {
		int alter;
		LocalDate heute = LocalDate.now();
		int jahr = heute.getYear();
		int monat = heute.getMonthValue();
		int tag = heute.getDayOfMonth();

		// Geburtsdatum ist null, alter -1
				alter = 16;
				kundeIni.setGeburtsdatum(null);
				assertEquals(kundeIni.getClass() + " hat das Alter " + alter, -1,
						kundeIni.berechneAlter());
				assertEquals(kundeIni.getClass()
						+ " Geburtsdaum wurde auf null gesetzt ", null,
						kundeIni.getGeburtsdatum());

		// 16 Jahre alt
		alter = 16;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat das Alter " + alter, -1,
				kundeIni.berechneAlter());
		assertEquals(kundeIni.getClass()
				+ " Geburtsdaum wurde auf null gesetzt ", null,
				kundeIni.getGeburtsdatum());

		// genau 18 Jahre alt
		alter = 18;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat genau das Alter " + alter,
				18, kundeIni.berechneAlter());

		// genau 18 Jahre minus 1 Tag alt
		alter = 18;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag + 1));
		assertEquals(kundeIni.getClass()
				+ " hat das Alter 1 Tag vor dem 18. Geb.", -1,
				kundeIni.berechneAlter());

		// genau 60 Jahre alt
		alter = 60;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat genau das Alter " + alter,
				alter, kundeIni.berechneAlter());

		// genau 60 Jahre minus 1 Tag alt
		alter = 60;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag + 1));
		assertEquals(kundeIni.getClass()
				+ " hat das Alter 1 Tag vor dem 60. Geb.", 59,
				kundeIni.berechneAlter());

		// 71 Jahre alt
		alter = 71;
		kundeIni.setGeburtsdatum(LocalDate.of(jahr - alter, monat, tag));
		assertEquals(kundeIni.getClass() + " hat das Alter " + alter, alter,
				kundeIni.berechneAlter());
	}


	@Test
	public void equalsInKundeVO() {


		kundetY = kundetX;
		kundeZ = kundetY;

		// For any non-null reference value x, x.equals(null) should return
		// false.
		assertFalse(
				"For any non-null reference value x, x.equals(null) should return false.",
				kundetX.equals(null));

		// It is reflexive: for any non-null reference value x, x.equals(x)
		// should return true.
		assertTrue(
				"It is reflexive: for any non-null reference value x, x.equals(x) should return true. ",
				kundetY.equals(kundetY));

		// It is symmetric: for any non-null reference values x and y,
		// x.equals(y) should return true if and only if y.equals(x) returns
		// true.
		assertTrue(
				"It is symmetric: for any non-null reference values x and y, x.equals(y) should return true if and only if y.equals(x) returns true. ",
				kundetX.equals(kundetY) == kundetY.equals(kundetX));

		// It is transitive: for any non-null reference values x, y, and z, if
		// x.equals(y) returns true and y.equals(z) returns true, then
		// x.equals(z) should return true.
		assertTrue(
				"It is transitive: for any non-null reference values x, y, and z, if x.equals(y) returns true and y.equals(z) returns true, then x.equals(z) should return true. ",
				(kundetX.equals(kundetY) && kundetY.equals(kundeZ)) ? kundetX
						.equals(kundeZ) : false);

		// Equals
		assertTrue(kundetY.getClass()
				+ " equals arbeitet bei gleichen Objekten richtig ",
				kundetY.equals(kundetX));

		kundeZ = new KundeVO("Anders", "Hans");
		assertFalse(kundeZ.getClass()
				+ " equals arbeitet bei ungleichen Objekten richtig ",
				kundeZ.equals(kundetX));

    Object obj = new PizzaVO();
		assertFalse(
				kundeZ.getClass()
						+ " equals arbeitet bei falschen Objekten (Nicht-"
						+ kundeZ.getClass() + " richtig ",
				kundeZ.equals(obj));
	}

	@Test
	public void equalsHashCodeInKundeVO() {
		kundetX = new KundeVO("Nachname", "Vorname");
		kundetY = kundetX;
		// Gleiche Objekte liefern den gleichen HashCode
		assertTrue("Gleiche Objekte liefern den gleichen HashCode",
				kundetX.equals(kundetY) == (kundetX.hashCode() == kundetY
						.hashCode()));

	}

}
