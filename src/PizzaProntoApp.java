import java.awt.Color;
import java.time.LocalDate;
import java.util.Arrays;

public class PizzaProntoApp {
  private LocalDate today;
  private LocalDate birthday;

  private KundeVO emptyCustomer;
  private KundeVO customerWithName;
  private KundeVO customerWithSex;
  private KundeVO customerWithBirthday;

  private PizzaVO emptyPizza;
  private PizzaVO pizzaSalami;

  private KochVO emptyCook;
  private KochVO cookNamed;

  private void initialize() {
    this.today = LocalDate.now();
    this.birthday = LocalDate.of(1990, 5, 24);

    this.emptyCustomer = new KundeVO();
    this.customerWithName = new KundeVO("Mustermann", "Max");
    this.customerWithSex = new KundeVO("Mustermann", "Max", "male");
    this.customerWithBirthday = new KundeVO("Mustermann", "Max", "male", birthday);

    this.emptyPizza = new PizzaVO();
    this.pizzaSalami = new PizzaVO("Salami", new String[] { "Salami", "Käse" }, 7.90f);

    this.emptyCook = new KochVO();
    this.cookNamed = new KochVO("Salvadore", "Luigi", Color.GREEN);
  }

  private void printReferenceVariables() {
    System.out.printf("All dates: %s, %s\n", this.today, this.birthday);
    System.out.printf("All customers: %s, %s, %s, %s\n", this.emptyCustomer, this.customerWithName, this.customerWithSex, this.customerWithBirthday);
    System.out.printf("All pizzas: %s, %s\n", this.emptyPizza, this.pizzaSalami);
    System.out.printf("All cooks: %s, %s\n", this.emptyCook, this.cookNamed);
  }

  private void setterTest() {
    this.emptyCustomer.setNachname("Mustermann");
    this.emptyCustomer.setVorname("Max");
    this.emptyCustomer.setGeschlecht("male");
    this.emptyCustomer.setGeburtsdatum(this.birthday);

    try {
      this.emptyPizza.setName("Hawaii");
      this.emptyPizza.setPreis(7.90f);
      this.emptyPizza.setZutaten(new String[] { "Ananas", "Schinken", "Käse" });
      this.emptyPizza.setPreis(-7.90f);
    } catch (IllegalArgumentException e) {
      System.out.printf("Can't set price, because %s\n", e.getMessage());
    }

    this.emptyCook.setNachname("Salvadore");
    this.emptyCook.setVorname("Mario");
    this.emptyCook.setFarbeSchuerze(Color.GREEN);
  }

  private void getterTest() {
    String customerName = this.emptyCustomer.getNachname();
    String customerPrename = this.emptyCustomer.getVorname();
    String customerSex = this.emptyCustomer.getGeschlecht();
    LocalDate customerBirthday = this.emptyCustomer.getGeburtsdatum();
    System.out.printf("values of emptyCustomer: {%s, %s, %s, %s}\n", customerName, customerPrename, customerSex, customerBirthday);

    String pizzaName = this.emptyPizza.getName();
    float pizzaPrice = this.emptyPizza.getPreis();
    String[] pizzaIngredients = this.emptyPizza.getZutaten();
    System.out.printf("values of emptyPizza: {%s, %s, %s}\n", pizzaName, pizzaPrice, Arrays.toString(pizzaIngredients));

    String cookName = this.emptyCook.getNachname();
    String cookPrename = this.emptyCook.getVorname();
    Color cookColor = this.emptyCook.getFarbeSchuerze();
    System.out.printf("values of emptyCook: {%s, %s, %s}\n", cookName, cookPrename, cookColor); }

  private void equalsTest() {
    System.out.printf("(not so) empty cook equals (not so) empty cook: %s\n", this.emptyCook.equals(this.emptyCook));
    System.out.printf("(not so) empty cook equals named cook: %s\n", this.emptyCook.equals(this.cookNamed));
    PizzaVO clone = this.emptyPizza.clone();
    System.out.printf("(not so) empty pizza equals cloned empty pizza: %s\n", this.emptyPizza.equals(clone));
    System.out.printf("(not so) empty pizza equals salami pizza: %s\n", this.emptyPizza.equals(this.pizzaSalami));
    System.out.printf("hashcodes of empty, clone, salami: %s, %s, %s\n", this.emptyPizza.hashCode(), clone.hashCode(), this.pizzaSalami.hashCode());
    System.out.println("hashcode of empty cook: " + new KochVO().hashCode());
  }

  public static void main(String[] args) {
    PizzaProntoApp tester = new PizzaProntoApp();
    tester.initialize();
    tester.printReferenceVariables();
    tester.getterTest();
    tester.setterTest();
    tester.getterTest();
    tester.equalsTest();
  }
}
