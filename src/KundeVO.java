import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.Getter;

@Data
public class KundeVO {
  @Getter
  private static int naechsteID = 0;

  @Setter(AccessLevel.NONE)
  private int id;
  private String nachname;
  private String vorname;
  private String geschlecht;
  private LocalDate geburtsdatum;

  public KundeVO(String nachname, String vorname, String geschlecht, LocalDate geburtsdatum) {
    this.id = naechsteID++;
    this.setNachname(nachname);
    this.setVorname(vorname);
    this.setGeschlecht(geschlecht);
    this.setGeburtsdatum(geburtsdatum);
  }

  public KundeVO(String nachname, String vorname, String geschlecht) {
    this(nachname, vorname, geschlecht, null);
  }

  public KundeVO(String nachname, String vorname) {
    this(nachname, vorname, null);
  }

  public KundeVO() {
    this(null, null);
  }

  public void setGeburtsdatum(LocalDate birthday) {
    this.geburtsdatum = birthday;
    this.geburtsdatum = (this.berechneAlter() < 18) ? null : this.geburtsdatum;
  }

  public String GeburtsdatumStr() {
    return this.geburtsdatum.format(DateTimeFormatter.ofPattern("dd.MMM.yyyy"));
  }

  public short berechneAlter() {
    short age = -1;
    LocalDate now = LocalDate.now();
    if (this.geburtsdatum != null) {
      age = (short) Period.between(this.geburtsdatum, now).getYears();
    }
    return age;
  }
}
