import java.awt.Color;
import java.lang.reflect.Field;
import java.util.function.BooleanSupplier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KochVO {
  private String nachname;
  private String vorname;
  private Color farbeSchuerze;

  // // simple solution
  // public boolean equals(Object obj) {
  //   return obj != null &&
  //     this.getClass() == obj.getClass() &&
  //     (
  //       this == obj ||
  //       (
  //         this.nachname.equals(((KochVO)obj).nachname) &&
  //         this.vorname.equals(((KochVO)obj).vorname) &&
  //         this.farbeSchuerze.equals(((KochVO)obj).farbeSchuerze)
  //       )
  //     );
  // }
  //
  // public int hashCode() {
  //   int hash = 0;
  //   hash += (this.nachname != null) ? this.nachname.hashCode() : 0;
  //   hash += (this.vorname != null) ? this.vorname.hashCode() : 0;
  //   hash += (this.farbeSchuerze != null) ? this.farbeSchuerze.hashCode() : 0;
  //   return hash;
  // }

  // reflection solution
  public boolean equals(Object obj) {
    BooleanSupplier iterate = () -> {
      Field[] fields = KochVO.class.getDeclaredFields();
      for (Field field : fields) {
        try {
          if(!(field.get(this).equals(field.get(obj)))) return false;
        } catch (IllegalArgumentException | IllegalAccessException e) {
          e.printStackTrace();
        }
      }
      return true;
    };
    return obj != null &&
      this.getClass() == obj.getClass() &&
      (this == obj || iterate.getAsBoolean());
  }

  public int hashCode() {
    int hash = 0;
    Field[] fields = KochVO.class.getDeclaredFields();
    for (Field field : fields) {
      Object value = null;
      try {
        value = field.get(this);
      } catch (IllegalArgumentException | IllegalAccessException e) {
        e.printStackTrace();
      }
      hash += (value != null) ? value.hashCode() : 0;
    }
    return hash;
  }
}
