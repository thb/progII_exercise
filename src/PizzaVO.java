import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PizzaVO {
  private String name;
  private float preis;
  private String[] zutaten;

  public PizzaVO(String name, String[] zutaten, float preis) throws IllegalArgumentException {
    setName(name);
    setPreis(preis);
    setZutaten(zutaten);
  }

  public PizzaVO clone() {
    PizzaVO pizza = new PizzaVO(this.name, this.zutaten, this.preis);
    return pizza;
  }

  public void setPreis(float preis) throws IllegalArgumentException {
    if(preis >= 0) {
      this.preis = preis;
    } else {
      throw new IllegalArgumentException(String.format("%s is no suitable price for a pizza.", preis));
    }
  }
}
